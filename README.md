## salis - Simple Arch Linux Installation Script

### What is this?
A simple Arch Linux installation script.

### Why? archinstall exists!
Simple, because we don't like archinstall.

### What are the benefits of salis?
- 100% scriptable from other scripts one writes (no interactive questions, just commandline argument parameters!)
- Small. salis keeps it lightweight. If something breaks, chances are good you can fix it yourself.
- Debuggable. salis uses "set -x" (with some beautification) to indicate every command it runs.
- Hackable. Code is written with the reader in mind and designed to be well readable.

### How do I use this?
Easy. You boot from an Arch Linux live medium, connect to the internet, obtain salis, and run it. Everything from here on will run 100% un-attended. It's that simple. Please take extra care, this does destructive action to your hard disk. If you don't know what you're doing, stop reading here and make sure you understand that running this will include wiping all data from your drive.

If you're actually feeling adventurous, well, we won't stop you, go ahead.

```bash
curl -s https://codeberg.org/armin/salis/raw/branch/master/salis | bash
```

As you might have noticed, this doesn't do anything harmful. So, if you really want to play around with it:

```bash
curl -s https://codeberg.org/armin/salis/raw/branch/master/salis > salis
chmod +x ./salis
./salis
```

This will get you a working salis in your current directory. You would not want to pollute your home directory on an everyday setup, but since this is a live environment, that should be fine.

### TODO, missing features?
- Full Disk Encryption support (LUKS)
- Support for EFI
- Support for filesystems other than ext4

### Contact
If you want to get in contact, feel free to ping us up in #unix.porn on the [HackInt IRC network](https://hackint.org/).

### License
salis is being released under the terms of the WTFPL (http://www.wtfpl.net/)


